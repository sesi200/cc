<html>
  <head>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/head.html'); ?>
    <title>About</title>
  </head>
  <body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/nav.html'); ?>
	<section class="content">
		<h1>About</h1>
		
		<h2>Who is this?</h2>
		<p>Hi there! I am Severin, a learning enthusiast, student and geek from Switzerland. On this site, I write about my personal learning system and related things that I’m interested in. I'm sure you will find out more about myself if you read other stuff on the site. For now, you can probably find the most information about me if you go to the <a href="/cccourse">course page</a> and see how the course came into existence.</p>
		
		<h2>What happens on this site?</h2>
		<p>In short: Whatever I feel like. I made a course and host some related things on here, occasionally I write something resembling a blogpost and also put it here somewhere. I'll see what happens over time.</p>
		
		<h2>This site looks terrible!</h2>
		<p>You know what? I like it. And it's so low-tech (and all hand-written) that it will survive the zombie apocalypse and still be readable. Also, if you want to know how to produce such a site, just right click and look at the source code. If you want to know more about why one would do this all by hand, <a href="https://neustadt.fr/essays/the-small-web/">here's a nice little article</a> on a similar site that explains it much better than I could.</p>
		
		<h2>How does this site work?</h2>
		<p>I hand-write everything on here. This site is actually run with git and <a href="https://stackoverflow.com/questions/13135601/proper-way-to-pull-git-production-branch-to-production-server">this script</a>. It's not fancy, certainly not high tech, but it works and it's a tool I know well enough. You can find the entire source for this site <a href="https://gitlab.com/sesi200/cc">right here</a> and even recommend fixes if you like to. You also might stumble upon some things that you don't expect to find in public...</p>
	</section>
  </body>
</html>