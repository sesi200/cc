<html>
  <head>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/head.html'); ?>
    <title>Direct Communication</title>
  </head>
  <body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/nav.html'); ?>
	<section class="content">
		<h1>Direct Communication</h1>
		<p>I see so often questions along the lines of "I recently encountered that wonderful person and they happen to be an autist. What do I need to keep in mind when talking to them?" Almost every time I go back to something I advised someone else and copy my advice almost word for word. So here it is once again, but in a (at least for me) central place.</p>
		
		<p>This is supposed to be advice about talking/living with autists (I don't write "people with autism" because <a href="https://autisticnotweird.com/autistic-person/">the majority prefers it the other way round</a>). However, I'm strongly of the opinion that this is not just advice about talking with autists, but instead advice on healthy communication in ANY relationship. So if you stumble upon this article in a different context, please don't dismiss it outright because of that.</p>
		
		<h2>Introduction</h2>
		<p>As an autist myself (diagnosed with Asperger's syndrome), I'm often asked about interacting with autists in general. Whenever that happens, I'm quite reluctant to give broad advice because of the often-repeated "If you've met an autist, you've met one autist." There's so many different ways autism manifests in people that it's really hard to find advice that is truly universal. The one thing I nowadays talk about in these situations is how most autists communicate: very, very directly. I've found this to be probably the most universal thing non-autists have a hard time adjusting to.</p>
		
		<p>From observing couples I know, I've actually come to the conclusion that communicating straightforward is the best way to keep relationships (not only when autists are involved) healthy. In my eyes, the closer a couple is to direct communication, the longer their relationship works well.</p>
		
		<h2>What is Direct Communication?</h2>
		<p>In short, it's "talking honestly, without hidden meanings and with little to no sugar-coating". It means that if you have an issue with something, you better tell them literally what you have a problem with. Don't drop subtle hints and get disappointed after they don't pick it up for weeks. It's answering honestly to questions, for example "How did you like dinner tonight?" If you say you liked it, then you will like it <em>exactly</em> that way the next 300 times. If not, say "It was nice, but if we did that a dozen more times, I'd prefer it that way" or "It was nice, but I'd love to try it out that other way the next time." Or if you liked it the first time, but after doing it for 25 times you want to change something, you have to speak up. If you don't explicitly change your answer, autists will tend to think that since you said you like it, your signs of discomfort are just an unrelated fluke.</p>
		
		<p>Non-autists are often taken aback by the bluntness autists talk with. Sadly, this bluntness is often misunderstood for a lack of care for the other person or their feelings. But it's not that we autists don't care about you. We just don't have the same priority of values as you. So if we tell you that "Your hair looked better yesterday" that does not mean that you're ugly, stupid, or we don't like you. It just means that yesterday, your hair looked better than today. And we say this not because we want to distress you, but because we think it's nice if we let you know that there's something you could fix. We care for you, even if you don't notice! And all the autists I know value (sometimes brutal) honesty and the truth more than temporary feelings.</p>
		
		<p>Whenever you're not sure about something, ask. As I just said, autists value honesty and truth very highly. If you're not sure about something, ask for their help to find the truth: "Are you feeling alright? You seem so tense today" or "What did you mean when you commented on my hair? I thought you were insulting me." Note that I already included a reason why you ask. We autists mostly read and express feelings in a different way, so keep your 'Why?' explicit too, at least at the beginning. It helps us understand how you think and makes it easier to communicate feelings in the future. Sometimes it's also useful to mention that not everything needs to have an answer <em>now</em>. I often forget that it's ok to say that I need some more time to find out where my stress comes from.</p>
		
		<h2>Discussing hard things</h2>
		<p>Discussing hard things (think hygiene, chores, expectations, and so on) is really tough to do right, but can make relationships much easier. In my experience, if you have a serious disagreement or fight with each other, then you should have more peaceful discussions about those hard things before they blow up.</p>

		<p>During the actual discussions, try to focus on the problem and not on your partner. If you don't try to address the problem, the discussion will grow into a fight and resolution becomes impossible. Along the same line: If you start discussing while emotions are already relatively high, you probably will fail. Try to discuss when things are not urgent. Autists especially tend to panic and shut down any communication (or completely explode) when they get overwhelmed. But, since they relatively often experience overwhelming feelings, they often know a fair bit about their triggers and how they react. So you just sit down before something needs discussing and lay out a protocol on how to discuss, so if someone needs to take a break from the discussion it won't be a surprise that might be misinterpreted as a refusal to continue discussing.</p>
		
		<p>Put some effort into coming up with ways to keep calm in difficult discussions. I know a couple that does all their hard discussions via email because one of them just cannot formulate sentences properly when they feel the pressure to respond within minutes. Other options to consider are:</p>
		<ul>
		<li>Not everything needs to be resolved on the first day. Continuing discussions the next day when necessary.</li>
		<li>Having signals to start a time-out to collect thoughts.</li>
		<li>Discussing during a walk.</li>
		<li>Have someone play neutral third party.</li>
		<li>Don't surprise each other with hard discussions. Announce them a bit in advance or ask when a good time to have them would be.</li>
		</ul>
		
		<h2>...but that's just one perspective</h2>
		<p>As I already mentioned, the saying goes "If you've met an autist, you've met one autist." Don't take my experience as gospel, even though I'm very sure that direct communication helps in any relationship. When in doubt, ask. And if an autist comes off as too blunt, before you get offended think (or even better: ask) about what they really wanted to say.</p>
		
		<p></p>
		<p><a href="index.php">Back to the list of autism articles.</a></p>
	</section>
  </body>
</html>