<html>
  <head>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/head.html'); ?>
    <title>How the Conquering Complexity with Anki course came into existence</title>
  </head>
  <body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/nav.html'); ?>
	<section class="content">
		<h1>How the Conquering Complexity with Anki course came into existence</h1>
		<p>I’ve always had it easy in school and didn’t really have to learn. But, as you know, school gets harder and harder every year, and once I hit university level, I hit a wall. <em>Hard.</em> Because then, just listening to the lectures wasn’t enough anymore. I had to learn on my own. But because I never really learned how to study or how to be disciplined about it, I had to rely on my motivation and almost failed. Slowly, I developed some discipline, which helped, but studying often required too much mental energy to do as much as I wanted to.</p>

		<p>Once I realized that the heavy brain power use was my problem, I began searching for a solution. After a long time of looking, experimenting, refining, and going back to the beginning, I finally found the combination of incremental reading and spaced repetition. With that combination, I have no trouble studying for multiple hours per day and still have energy left over to do something else like writing for this site or learning about topics I am interested in privately. Even when my motivation is really bad, if I sit down, I make solid progress simply by letting the system do its work.</p>
		
		<p>After I was happy with what I created, I of course had to share it with my friends. They got tired at some point but I still felt like I had to share my system with more people. Some night I got the urge to create <a href="/red.php?t=0">the course</a>. I wrote the first version of the script within one sitting, and some months later I published the first version. And this is how it all began...</p>
	</section>
  </body>
</html>