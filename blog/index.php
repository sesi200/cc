<html>
  <head>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/head.html'); ?>
    <title>Blog</title>
  </head>
  <body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/nav.html'); ?>
	<section class="content">
      <h1>Blog</h1>
		<p>Occasionally, I write something that resembles a blogpost. Here's a list of those:</p>
		<p><a href="exam-preparation.php">Exam Preparation</a></p>
		<p><a href="context-switches.php">Context Switches: Costs and Benefits</a></p>
		<p><a href="deck-management.php">Anki deck management</a></p>
		<p><a href="why-you-need-a-learning-system.php">Why you need a learning system.</a></p>
		<p><a href="how-the-course-came-to-be.php">How the Conquering Complexity with Anki course came into existence.</a></p>
		<p><a href="cca-description.php">What is covered in the course?</a></p>
	</section>
  </body>
</html>