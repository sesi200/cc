<html>
  <head>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/head.html'); ?>
    <title>Page Title</title>
  </head>
  <body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/nav.html'); ?>
	<section class="content">
		<h1>Using mnemonics to remember <em>those</em> cards</h1>
		<p>Most flashcards you can remember rather quickly, but everyone has those few cards they simply cannot ever get right. Ih this article, I'll show you how you can use mnemonics to finally remember <em>those</em> cards.</p>
		
		<h2>What is a mnemonic?</h2>
		<p>A mnemonic is a memory help. Take something that is hard to memorize - say, the alphabet in order - and connect it with something that is easy to remember - a melody. There you go, that's a mnemonic. Mnemonics don't have to use a song; they can work with whatever a brain can easily remember: pictures, arbitrary patterns, or acronyms. Anything that helps you remember something with the help of something unrelated counts as a mnemonic.</p>
		
		<p>There's a variety of different mnemonics one can use, but those can be collected in quite few categories. Here's the ones I personally find useful:</p>
		<ul>
			<li>Adding a Melody, just like the alphabet song</li>
			<li>Inventing/using a person's name as an acronym for something else. TODO: example rainbow?</li>
			<li>Sentence with starting letters. TODO: example planets?</li>
			<li>Rhymes/little poems. TODO: example</li>
			<li>Creating crazy images. TODO: example see below?</li>
			<li>Finding a pattern. For example loNgitude is for the N/S axis because it contains an N</li>
		</ul>
		
		<p>To create a mnemonic, pick a thing you have trouble remembering and come up with some strange ways to connect it to something that's easier to remember. Find a pattern or rhyme, an acronym or an outrageous image to connect it with. The more surprising, the better. Brains are great at remembering weird or unnatural things. Outrageous and sexual pictures already were described as particularly memorable a thousand years ago. Our brains haven't changed much in the last thousand years, so just go ahead and picture a naked Oprah juggling a bunch of fruit to memorize that one weird smoothie recipe.</p>
		
		In my experience, the melody mnemonic is the strongest one, but sadly also the most expensive one to create. To see why, take a look at the one melody mnemonic everyone knows: the alphabet song. Imagine how long it would take you to create that from scratch and train it until you know it by heart. Luckily, there are things many people struggle with and so certain themes are covered with somewhat well-known songs. Example: the periodic table song. On the other hand, once you know a mnemonic song, you won't forget it for a long time. I'd bet almost anything that most grandparents still remember the alphabet song.
		
		
	</section>
  </body>
</html>