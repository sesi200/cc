<html>
  <head>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/head.html'); ?>
    <title>Why you need a learning system</title>
  </head>
  <body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/nav.html'); ?>
	<section class="content">
		<h1>Why you need a learning system</h1>
		<p>There are many reasons why ‘just learning’ is not a proper learning system and will not help you get to your learning goals. In this post, I will explain what benefits a properly designed system (for example Spaced Repetition) brings to your life.</p>
		
		<h2>Why 'just learning' does not work</h2>
		<p>If you ask a random person how they go about learning for an exam, most likely they will answer with one of the following:
		<ul>
			<li>Umm, I just read through the parts I don’t properly understand.</li>
			<li>I go through the material the evening before the exam. (A.k.a. <a href="https://www.urbandictionary.com/define.php?term=bulimic%20learning">bulimic learning</a>)</li>
			<li>Well, I do some exercises beforehand.</li>
		</ul>
		These and many other systems suffer from similar problems: Because there is no method to the madness, people have no overview about which material is learned already. It’s easy to waste effort on topics that you mastered already. Most systems only cover reading OR solving problems.</p>
		
		<h2>What you can expect from a learning system</h2>
		<h3>You save brain power</h3>
		<p>If you use a proper learning system, you don’t need to manage topics in your head. You don’t need to keep in mind which parts you already learned and which ones are still outstanding. Since your learning system keeps track of that for you, you can focus on actual learning. Also, there’s no need to think about where to store things when you’re done learning. Things don’t just pile up on your desk.</p>
		
		<h3>You save lots of time</h3>
		<p>My biggest issue with ‘just learning’ is with the sheer volume of duplicated effort. Even if they know 99% of the material in a chapter, most ‘just learers’ still repeat the entire text. By focussing your efforts on your weaknesses, a learning system guides you to the parts that need the most training. In addition, if you know that your efforts are in the places they’re most needed, you are much more confident in your abilities.</p>
		
		<h3>It’s easier to form learning habits</h3>
		<p>What do you think is more attractive to use? A pile of (unorganized?) material or a system you trust? Obviously, a reliable learning system is much more appealing. If you have such a system (I use Incremental Reading to learn new things and Spaced Repetition to keep my memory sharp), there’s nothing that stops you from learning. No collecting relevant chapters, no searching for sample solutions. With just a pile of material ready for use it is much harder to make excuses why you don’t learn right now.</p>
		
		<h3>You can experiment with variations</h3>
		<p>If you have your system and use it daily, you are almost certain to notice things you like and things to improve. Then, it’s easy to test some variations for a week or two and you can optimize the process slowly. Even after many months of steady improvements, I still try new things on an weekly basis. And that’s another wonderful thing about a system: It’s really easy to make small changes to the workflow and stick with it. In a ‘just learning’ system, there’s no way you can keep all your optimizations in mind.</p>
		
		<h2>Summary</h2>
		<p>Here’s the short version why you need a proper learning system:

		Brain power: Having your learning in one place makes it easy to keep track of what you need to learn.
		<ul>
			<li>Time saved: You don’t duplicate learning efforts on topics you already know.</li>
			<li>Less friction: If you are ready to learn within seconds, there’s just a lot less excuses that keep you from building proper learning habits.</li>
			<li>Constant improvements: If you have to set up your learning process from scratch every time, there’s no way you think about process improvements. When you improve your technique by a small bit every week, you will go very far within even just a year.</li>
		</ul></p>
	</section>
  </body>
</html>