<html>
  <head>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/head.html'); ?>
    <title>Course Downloads</title>
  </head>
  <body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/nav.html'); ?>
	<section class="content">
		<h1>Course Downloads</h1>
		<p>Welcome to the course downloads. Here you can find the mentioned Syllabus and the bookmarks. Once I received enough questions about the course material I will also add an FAQ here.</p>
		<h2><a href="course-downloads/syllabus.pdf">Syllabus</a></h2>
		<h2><a href="course-downloads/cc-course-bookmarks.html.zip">Bookmarks file</a></h2>
	</section>
  </body>
</html>