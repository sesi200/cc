<html>
  <head>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/head.html'); ?>
    <title>Conquering Complexity with Anki</title>
  </head>
  <body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/nav.html'); ?>
	<section class="content">
		<h1>Conquering Complexity with Anki</h1>
		
		<p>Do you need a system to get your learning sorted out?</p>
		<p>Are you overwhelmed with past material to keep up with?</p>
		<p>Do you have way too many things you want to learn at the same time?</p>
		<p>Are you in need of a solid strategy to ace that upcoming exam?</p>
		<p>Those were my exact problems at the beginning of my studies; and today, I have the solution for all those problems. And I want to teach it to you!</p>
		
		<p>Here's how my daily learning looks like:
		<ul>
			<li>I spend 5-20 minutes keeping up with what I've learned over the past year or so.</li>
			<li>Afterwards, I process all new material from university and put it in my spaced repetition system (Anki), so that I am prepared for the finals without doing any extra learning.</li>
			<li>Then, I keep learning new things for as long as I want to. I am reading up to 50 books/articles at the same time and can keep up without any problem.</li>
		</ul></p>
		
		<h2>What you get from the course</h2>
		<p>After the <a href="/red.php?t=0">course</a>, you have
		<ul>
			<li>A system to learn anything you want.</li>
			<li>A system to keep your knowledge current.</li>
			<li>Solid understanding of how and why it works.</li>
		</ul>
		The Conquering Complexity Course teaches you anything you need to start learning as much as you want and whatever you want in three parts. The first part covers the theoretical basics:
		<ul>
			<li>How memory works</li>
			<li>How to improve your memory with Spaced Repetition</li>
			<li>How to easily learn with Incremental Reading</li>
		</ul>
		Afterwards, the practical part guides you through setup and use of your own incremental reading and spaced repetition system. Then, you are already ready to learn on your own. The practical section covers:
		<ul>
			<li>Installing/configuring Anki and the required plugins</li>
			<li>Importing reading material</li>
			<li>Creating flashcards</li>
			<li>Incremental Reading, including multiple demonstrations</li>
		</ul>
		The third part is there to make your life easier. It explains the details of spaced repetition and incremental reading. We take a look at all the nitty-gritty details, so you can get the most out of your time:
		<ul>
			<li>What makes a flashcard valuable?</li>
			<li>Making cards memorable.</li>
			<li>Finding and improving bad cards.</li>
		</ul>
		You can get the course <a href="/red.php?t=0">right here</a>. If you want more information, you can read more <a href="/blog/cca-description.php">over here</a>.
		</p>
	</section>
  </body>
</html>