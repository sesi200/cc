<html>
  <head>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/head.html'); ?>
    <title>Home</title>
  </head>
  <body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/res/nav.html'); ?>
	<section class="content">
      <h1>Welcome to conqueringcomplexity.net</h1>
		<p>Welcome to my little site! Here's what you can take a look at:</p>
		<p>The <a href="about.php">About</a> page. It explains who I am, why this site looks like it comes from the last millennium, and how it works behind the scenes.</p>
		<p>The <a href="/blog">Blog</a> page. Occasionally I write something that resembles a blogpost. Over there you can see if any of them wake your interest.</p>
		<p>The <a href="/cccourse">Conquering Complexity with Anki</a> page. I made a <a href="/red.php?t=0">Udemy course</a> about my own learning system. If you want to know how I use Anki and Incremental Reading to study anything, take a look over there.</p>
	</section>
  </body>
</html>