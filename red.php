<?php
	function redirect($goal) {
		header('Location: '.$goal, true, 303);
		exit();
	}

	$target = $_GET["t"];
	switch($target) {
		case 0:
			//Conquering Complexity with Anki base link
			redirect("https://www.udemy.com/course/conquering-complexity/?referralCode=743DED10A960F48B63AD");
		default:
			redirect("/index.php");
	}
?>
