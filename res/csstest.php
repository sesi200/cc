<html>
  <head>
	<link rel="StyleSheet" href="style.css" type="text/css">
    <title>Anki deck management</title>
  </head>
  <body>
	<nav class="sidebar">
		<a href="../index.php"><img src="../img/logo.png" style="max-width:100%; height: auto;"/></a>
	  <ul>
		<li><a href="/about.php">About</a></li>
		<li><a href="/blog">Blog</a></li>
		<li><a href="/cccourse">Conquering Complexity with Anki</a></li>
	  </ul>
	</nav>
    <section class="content">
		<h1>Anki deck management</h1>
		<p>In most spaced repetition systems you can organize the flashcards in various collections, usually called decks. Anki even allows entire hierarchies of decks, going many levels deep. Most people never closely think about how they manage their flashcards. They just start creating decks and add a sub-deck whenever they feel like adding one. In this post, we will have a look at different deck management approaches.</p>
		
		<h2>A Deck Per Subject Matter</h2>
		<p>Probably the most popular way of organizing decks is creating a deck for every area of knowledge. If you are taking classes, separating by class is exactly that. My first attempts with Anki were also precisely that way. The biggest benefit of having one deck per subject matter is that it makes it easy to study whatever you feel is most important; especially straight before an exam.</p>
		<p>Sadly, as soon as that exam is over, barely anyone I asked continued studying their cards, and eventually the deck was deleted. When asked why, the most common answer is that people don’t feel like that subject will be worth maintaining. So the easiest decision is to just delete all those cards. And all that time invested in learning is just wasted.</p>
		<p>Usually, maintaining an acceptable level of competence in an area takes barely any time at all, especially when you delete unnecessary details and only maintain the bigger picture. As an example: Now, two weeks into 2020, it costs me three to five minutes per day to keep up with all my 3500 cards from 2019.</p>
		<p>The other issue I have with narrow decks is that it makes it almost impossible to cross-connect knowledge. When you study a deck you are in the mindset of “let’s think only about this subject because everything else is irrelevant”. This actively discourages thinking about how a concept is similar to concepts you already know under different names. Because you miss that relationship, you then need to learn the same concept multiple times.</p>
		
		<h2>A Deck Per Time Period</h2>
		<p>Another structure I tried out after the subject matter decks is splitting by university semesters. Again, this makes it really easy to study for an exam period. But this organization had the same effect on me as the subject matter decks had: A few weeks after the last exam, I thought “These cards are all useless now,” and I deleted them.</p>
		<p>Deleting cards because they seem old is similar to the problem from the subject matter decks: You lose so much knowledge that you could have maintained with minimal effort. In addition, if you learn stuff that builds up on knowledge from previous periods, suddenly you are missing the basis for that new knowledge and risk crashing your entire mental model because of a missing foundation.</p>
		
		<h2>One Deck To Rule Them All</h2>
		<p>By now, my personal favourite deck management method is this one: Have one deck for everything. Sounds terrible? It’s actually great, but hear me out before you dismiss this idea! First of all, it’s simple:</p>
		<p>I have one single deck called “Long Term Storage” that I study daily, and that’s it. It’s a single task (instead of having to go through ten different decks), and it’s quite exciting to go through. I encounter things I didn’t even know anymore that I ever learned them. Additionally, I don’t get bored of the 25th card about mathematical definitions because other, more interesting subjects break up the routine.</p>
		<p>Then, mixing up subjects gives me the opportunity to discover connections: If two similar things pop up close together, I will ask myself “Is this the same thing, or is there a difference?” And if I can’t answer easily, I just discovered a hole in my knowledge that should be plugged with a new flashcard.</p>
		<p>If you have objections about losing your organization and about never ever finding your cards again, I got you covered: You can still accomplish the same as the two methods beforehand! Just use <a href="https://apps.ankiweb.net/docs/manual.html#searching">Anki’s tagging system</a> to find your cards again. My cards for classes usually have the following tags:
		<ul>
			<li>tag to signify that it’s from university</li>
			<li>a tag to signify the semester, e.g. Fall19 for the fall semester in 2019</li>
			<li>a tag for the class, e.g. DataStructures</li>
		</ul>
		This way, I can still do the same as above with filtered decks, but I also have the cross-connecting benefits of having one big pile of cards.</p>
		
		<h2>Summary</h2>
		<p>My personal preference for deck management is having a single deck so that you can notice similarities in related concepts. Then you use tags to know where the cards come from. Because of the tags, you can accomplish everything you can accomplish with the other methods almost as easily.</p>
	</section>
  </body>
</html>